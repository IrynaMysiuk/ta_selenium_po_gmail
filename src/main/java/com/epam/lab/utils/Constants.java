package com.epam.lab.utils;

public class Constants {
    public static final String URL = "https://www.google.com";
    public static final String GMAIL = "Gmail";
    public static final int SHIFT = 1;
    public static final int START_WAIT_TIME = 20;
    public static final int WAIT_TIME_OUT = 40;

    private Constants() {

    }
}
